package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"bitbucket.org/dhakobyan/smart-edge/lib"
)

func main() {
	params := os.Args
	if len(params) != 2 {
		log.Println("Input parameter is not specified")
		os.Exit(1)
	}
	email := params[1]
	if len(email) > 250 {
		fmt.Println("Input parameter can contains up to 250 characters.")
		os.Exit(1)
	}

	// Generate private/public keys if doesnt exist
	pk, _ := lib.GenerateKeyPair()

	response := pk.SignAndValidateSignature(email)
	printJson(response)
}

func printJson(response lib.Response) {
	bytes, err := json.Marshal(response)
	if err != nil {
		log.Printf("could not sign request: %v", err)
	}
	b, _ := prettyprint(bytes)
	fmt.Printf("%s", b)
}

func prettyprint(b []byte) ([]byte, error) {
	var out bytes.Buffer
	err := json.Indent(&out, b, "", "  ")
	return out.Bytes(), err
}

