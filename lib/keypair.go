package lib

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/asn1"
	"encoding/pem"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

const (
	bitSize        = 2048
	privateKeyFile = "private.pem"
	publicKeyFile  = "public.pem"
)

var parentDir string

func init() {
	exe, err := os.Executable()
	if err != nil {
		log.Fatalf("could not find path of executable: %v", err)
	}
	exeDir := filepath.Dir(exe)
	parentDir = filepath.Join(exeDir, "keypair")
}

func fileExists(keyPath string) bool {
	if _, err := os.Stat(keyPath); err!=nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

// Subsequent invocations of the method should read from the same files
func GenerateKeyPair() (*rsaPrivateKey, error) {
	privateKeyPath := filepath.Join(parentDir, privateKeyFile)
	publicKeyPath := filepath.Join(parentDir, publicKeyFile)

	if !fileExists(parentDir) {
		if err := os.MkdirAll(parentDir, os.ModePerm); err != nil {
			log.Fatalf("could not create directory %s: %v", parentDir, err)
			return nil, err
		}
	}

	if fileExists(privateKeyPath) && fileExists(privateKeyPath) {
		return loadPrivateKey(privateKeyPath), nil
	}

	privateKey, err := rsa.GenerateKey(rand.Reader, bitSize)
	if err != nil {
		log.Fatalf("Fatal error: %v", err.Error())
		return nil, err
	}
	if err := savePrivateKey(privateKey, privateKeyPath); err != nil {
		return nil, err
	}
	if err := savePublicKey(privateKey.PublicKey, publicKeyPath); err != nil {
		return nil, err
	}
	return &rsaPrivateKey{privateKey}, nil
}

func savePrivateKey(prevkey *rsa.PrivateKey, filename string) error {
	pemfile, err := os.Create(filename)
	if err != nil {
		log.Fatalf("could not create file %s: %v", filename, err)
	}
	defer pemfile.Close()

	// Convert it to pem
	var pemkey = &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(prevkey),
	}

	if err = pem.Encode(pemfile, pemkey); err != nil {
		log.Fatalf("unable to create PrivateKey file: %v", err)
	}
	return nil
}

func savePublicKey(pubkey rsa.PublicKey, filename string) error {
	pemfile, err := os.Create(filename)
	if err != nil {
		log.Fatalf("could not create file %s: %v", filename, err)
	}
	defer pemfile.Close()

	asn1Bytes, err := asn1.Marshal(pubkey)
	if err != nil {
		log.Fatalf("unable to create PublicKey file: %v", err)
		return err
	}

	var pemkey = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: asn1Bytes,
	}

	if err = pem.Encode(pemfile, pemkey); err != nil {
		log.Fatalf("unable to create PublicKey file: %v", err)
	}
	return nil
}

// Decodes an RSA key, expected to be in PEM format, from the specified filename.
func loadPrivateKey(privateKeyPath string) *rsaPrivateKey {
	data, err := ioutil.ReadFile(privateKeyPath)
	if err != nil {
		log.Fatalf("could not read file %s: %v", privateKeyPath, err)
	}
	block, _ := pem.Decode(data)
	privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		log.Fatalf("could not parse key: %v", err)
	}
	return &rsaPrivateKey{privateKey}
}

func loadPublicKey(filename string) *rsa.PublicKey {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("could not read file %s: %v", filename, err)
	}
	block, _ := pem.Decode(data)
	publicKey, err := x509.ParsePKCS1PublicKey(block.Bytes)
	if err != nil {
		log.Fatalf("could not parse key: %v", err)
	}
	return publicKey
}
