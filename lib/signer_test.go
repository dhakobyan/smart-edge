package lib

import (
	"crypto/x509"
	"encoding/pem"
	"testing"
)

var (
	privateKeyStr = `-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAxSv/jWAR/PUyaOz92Sfe7AsTDPoWsU/+TdH02f+EtpxbNSmI
EIUjUq3Au/BpKl0RFL+FYyZQ05pzsAukBqt1vIuJ2pxCmacdNei9qsf5uZgZTGLE
G242kxlto7uUvCbn0zvDfpkadsH/cciawwLEt9Gpo3QYZHGqgjGd1RVcFdKrS57I
ATQJeJykBKC7GsU0otEA0sOnGmNRyYxiwkakiREIYEo0DQr/voPi+SktvqUzAL1j
/ljjKRY8xUk1KpoqX1knSjfoa8Z3w58j5oRUYNMnJU0Qb8KBQn6CrPytw6YvSItK
om21v4wl4C8kdg0Hb+BwIk9Ed1n1z7wWsv538wIDAQABAoIBAG68ajc1WITwFNK3
H06BxQJeqeFk56/H1HRSUajJh/ijUdpGrANTMuZxbUhgGkBPqpMziS+JSmW9XfHm
Z8XTMDmnSNH9Zq4zHAW15CfxTgB9OuAarDcXrBDrfBcz97HAS/znmMEWLbW6+MMn
vjt954uxdgu13Sk0aWXbheAxsdkSzsp98hdhzUX4SwUUoTyR7AcnScdkuHgQTyQ6
uanafmiTLB1zgfK3YL3jJ2CfpRV3W6QwlD1oSYTx6FZ8tIbg+G5E+z6GEPBGIZZf
K0SOvxmM19H1Wpp2Ot22tL2Sb0DVcnAAaiY04XWz2s4vnHoSx4ETeS39vf1YTS2E
WsAn/MkCgYEA6kUYXkOslPbUxEv1NWc8eplAWZFRwXsDuiiJzW951M+lHJPbc6ee
QGUTZn6/hBydMZ3UYvrp38g34Vzt+QiN6H8WOl7wiEqjGotk2sLkgrqH318cfTpu
/uzhZikU6prPHNjMWmt2h+DzCZyt1Y/orNjuJgPJLy49SpU7f5QDPWcCgYEA13X7
46YXQZizfsi+8/CXuZPqaSCHiRla8BafmP+roFhSrQYH/+cNs5RPFeXCViRXPsh2
JI/ASQMhYeFT7oz3eRk/3JIhwjIElp4NsQRW9zLaIXBBzl0RwmRnSC2VSCeseft9
oMLa8YIc1NVha8PoH/HEgG3cvPQ2/Ztu5sEmjZUCgYAH9BqUsLV+RTcKBw4nJv8d
pnBR2iYtbHC+9DthdyRqvjX712U+8vTjeMsE0s7LvHD1y0b3qU8hYtnK0+3cY5b1
ysLFbo26AtawwwTEjJNXgN+2DLOZ6M715rLsVf64zFK1xwKBG17l1BRMQj6Anqcm
nyLOtZgx+t7n1lMzzKGjhwKBgHoOZmFyB56raJOcGUCLvQfuGU7wlJc/+9oilsrP
m2Q8ZKmLzyBSvdpLW/nlrI+sCpiQaw0POjiiQ1j1ktyCkvbRLPJA/krKlvrUEqGC
+dkTv6pNs5m584OR6UhkOjsj7nsz0tktSWai9L5FGuk3k9n01gKDOhb4qeZFFTA3
nyV5AoGANVgKIKg8vnxTRGnbxstCw8GtbpH5EtukyGl/QWV/CyOcJSHfuUEf8iKB
GWDlFqMu8+6mPBN6D9JY0FgC+dlO0l8tp2bBFnoEffgN9TSJOsmocrOY8pI31ZON
o2R/s6aRS8mnuXRwWxkPXt6Xab7KG4hq2cWwY3S4PwDUQQsGRak=
-----END RSA PRIVATE KEY-----`

	publicKey = `-----BEGIN PUBLIC KEY-----
MIIBCgKCAQEAxSv/jWAR/PUyaOz92Sfe7AsTDPoWsU/+TdH02f+EtpxbNSmIEIUj
Uq3Au/BpKl0RFL+FYyZQ05pzsAukBqt1vIuJ2pxCmacdNei9qsf5uZgZTGLEG242
kxlto7uUvCbn0zvDfpkadsH/cciawwLEt9Gpo3QYZHGqgjGd1RVcFdKrS57IATQJ
eJykBKC7GsU0otEA0sOnGmNRyYxiwkakiREIYEo0DQr/voPi+SktvqUzAL1j/ljj
KRY8xUk1KpoqX1knSjfoa8Z3w58j5oRUYNMnJU0Qb8KBQn6CrPytw6YvSItKom21
v4wl4C8kdg0Hb+BwIk9Ed1n1z7wWsv538wIDAQAB
-----END PUBLIC KEY-----`
	email = "your@email.com"
	signature = "T1UaWyvxUiNsIB1pDFiyVNo3ejNh9Buzoo1BO43Nctqsi5RRtEDDbUfMXrfPLDDUpzdRQxh7FfJVnWG0FFjKTOq+uGrodi2M4oKaVOU+FVK7G8Cpge2pQFFm7CfUI6KWBQI9tPOxAFU9y9N/WXGzy9SpYgVETjb/cYzbHdM+GhThaFjMknpm7lBGBCqxCSgxH8MNMFemzUoTmedQpOqvIwUV1mVr06D0l4QY9nHfMwCCCmusMIzB+qKFzdI1SmPFsZQxQM9r3mx65VXZEds0UXjeA2uCnNRWTQTed+NipmFwSfc9qMTdVsv3HMPQwVYqChITKbNHLNq/t+oHSnhhuA=="
)

func TestSignature(t *testing.T) {
	block, _ := pem.Decode([]byte(privateKeyStr))
	if block == nil {
		t.Fail()
	}
	privateKey, err2 := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err2 != nil {
		t.Fail()
	}
	pk := rsaPrivateKey{privateKey}

	actual := pk.SignAndValidadteSignature(email)

	if signature != actual.Properties.Signature {
		t.Fail()
	}
}