package lib

import (
	"bytes"
	"io/ioutil"
	"os"
	"testing"
)

func TestGenerateKeyPair_identical(t *testing.T) {
	// generate privateKey 1
	privateKey1, err := GenerateKeyPair()
	if err != nil {
		t.Fail()
	}
	privateKeyFile1 := tempFile(t)
	defer privateKeyFile1.Close()
	// Save PrivateKey into temp file 1
	bytes1 := saveAndGetBytes(t, *privateKey1, privateKeyFile1.Name())

	// generate privateKey 1
	privateKey2, err := GenerateKeyPair()
	if err != nil {
		t.Fail()
	}
	privateKeyFile2 := tempFile(t)
	defer privateKeyFile2.Close()

	// Save PrivateKey into temp file 2
	bytes2 := saveAndGetBytes(t, *privateKey2, privateKeyFile2.Name())

	// compare the byte array
	if bytes.Compare(bytes1, bytes2) != 0 {
		t.Fatal("bytes are not identical")
	}
}

func TestSaveAndLoadPrivateKey(t *testing.T) {
	privateKey, err := GenerateKeyPair()
	if err != nil {
		t.Fail()
	}

	// Save PrivateKey into temp file
	pkFile1 := tempFile(t)
	defer pkFile1.Close()
	pkBytes1 := saveAndGetBytes(t, *privateKey, pkFile1.Name())

	// Load the same PrivateKey from temp file
	loadedPrivateKey := loadPrivateKey(pkFile1.Name())

	// Save PrivateKey into different temp file
	pkFile2 := tempFile(t)
	defer pkFile2.Close()

	// use the loaded PrivateKey to save into another temp file
	pkBytes2 := saveAndGetBytes(t, *loadedPrivateKey, pkFile2.Name())

	// compare the byte array
	if bytes.Compare(pkBytes1, pkBytes2) != 0 {
		t.Fatal("bytes are not identical")
	}
}

func saveAndGetBytes(t *testing.T, rsa rsaPrivateKey, file string) []byte {
	err := savePrivateKey(rsa.PrivateKey, file)
	if err != nil {
		t.Fatal()
	}

	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		t.Fail()
	}
	return bytes
}

func tempFile(t *testing.T) *os.File {
	file, err := ioutil.TempFile("", "")
	if err != nil {
		t.Fail()
	}
	return file
}