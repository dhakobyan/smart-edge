package lib

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"log"
)

type rsaPrivateKey struct {
	*rsa.PrivateKey
}

// Sign signs data with rsa-sha256
func (pk *rsaPrivateKey) sign(data []byte) ([]byte, error) {
	h := sha256.New()
	h.Write(data)
	d := h.Sum(nil)
	return rsa.SignPKCS1v15(rand.Reader, pk.PrivateKey, crypto.SHA256, d)
}

// validate verifies the message using a rsa-sha256 signature
func (pk *rsaPrivateKey) validate(data []byte, sig []byte) error {
	h := sha256.New()
	h.Write(data)
	d := h.Sum(nil)
	return rsa.VerifyPKCS1v15(&pk.PrivateKey.PublicKey, crypto.SHA256, d, sig)
}

// SignEmailValidadteSignature generates a signature using the provided RSA key and the SHA256 digest of the input.
func (pk *rsaPrivateKey) SignAndValidateSignature(email string) Response {
	data := []byte(email)
	sig, err1 := pk.sign(data)
	if err1 != nil {
		log.Fatalf("unable to sign message: %v", err1)
	}
	if err2 := pk.validate(data, sig); err2 != nil {
		log.Fatalf("unable tp validation rsa signature :%s: %v", email, err2)
	}

	pemBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: x509.MarshalPKCS1PublicKey(&pk.PrivateKey.PublicKey),
	})

	response := Response{
		Schema:      "http://json-schema.org/draft-04/schema#",
		Title:       "Signed Identifier",
		Description: "Schema for a signed identifier",
		Type:        "object",
		Required:    []string{"message", "signature", "pubkey"},
		Properties: Property{
			Message:   email,
			Signature: base64.StdEncoding.EncodeToString(sig),
			Pubkey:    string(pemBytes),
		},
	}
	return response
}

type Response struct {
	Schema      string   `json:"$schema"`
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Type        string   `json:"type"`
	Required    []string `json:"required"`
	Properties  Property `json:"properties"`
}

type Property struct {
	// description": "original string provided as the input to your app
	Message string `json:"message"`

	// RFC 4648 compliant Base64 encoded cryptographic signature of the input, calculated using the private key and the SHA256 digest of the input
	Signature string `json:"signature"`

	// Base64 encoded string (PEM format) of the public key generated from the private key used to create the digital signature
	Pubkey string `json:"pubkey"`
}