# SmartEdge codechallenge

## Start the app
```
docker build . -t codechal
docker run codechal "your@email.com"
```

## Reference OpenSSL usage
 
* Generate a 2048 bit RSA Key
 
 ```openssl genrsa -out private.pem 2048```

* Export the RSA Public Key to a File
 
 ```openssl rsa -in private.pem -outform PEM -pubout -out public.pem```
 
 * Sign the input email
 
 ```openssl rsautl -sign -inkey private.pem -in email.txt -out email.rsa```
 
 * Verify signature
 
 ``` openssl dgst -sha256 -verify public.pem -signature email.rsa email.txt```
 
 * Extract the string from the signature
 
 ```openssl rsautl -verify -in email.rsa -inkey private.pem```